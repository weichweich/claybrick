use super::Trailer;

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum XReferenceKind {
    Table,
    Stream { number: u32, generation: u32 },
}

/// References to objects inside a PDF section.
///
/// References in this table mark object indices either as used or unused.
/// Unused object indices may be reused for new objects. Used objects are
/// divided into two groups compressed and uncompressed objects. Uncompressed
/// objects can be imidiately accessed at the given byte offset while compressed
/// objects are contained inside a stream object.
///
/// The entries are sorted by the object index.
#[derive(Debug, Clone, PartialEq)]
pub struct XSectionReference {
    /// The entries of the cross reference
    pub(crate) entries: Vec<XReferenceEntry>,

    /// The type of the cross reference table.
    pub(crate) kind: XReferenceKind,

    /// The trailer information.
    pub(crate) trailer: Trailer,
}

impl XSectionReference {
    pub(crate) fn new(mut entries: Vec<XReferenceEntry>, trailer: Trailer, kind: XReferenceKind) -> Self {
        entries.sort_by_key(|o| o.number());
        XSectionReference { entries, kind, trailer }
    }

    pub(crate) fn new_table(entries: Vec<XReferenceEntry>, trailer: Trailer) -> Self {
        Self::new(entries, trailer, XReferenceKind::Table)
    }

    pub(crate) fn new_stream(entries: Vec<XReferenceEntry>, number: u32, generation: u32, trailer: Trailer) -> Self {
        Self {
            entries,
            kind: XReferenceKind::Stream { number, generation },
            trailer,
        }
    }

    pub fn used_objects(&self) -> impl Iterator<Item = &UsedObject> {
        self.entries.iter().filter_map(|entry| {
            if let XReferenceEntry::Used(u) = entry {
                Some(u)
            } else {
                None
            }
        })
    }

    pub fn compressed_objects(&self) -> impl Iterator<Item = &UsedCompressedObject> {
        self.entries.iter().filter_map(|entry| {
            if let XReferenceEntry::UsedCompressed(u) = entry {
                Some(u)
            } else {
                None
            }
        })
    }

    pub fn free_objects(&self) -> impl Iterator<Item = &FreeObject> {
        self.entries.iter().filter_map(|entry| {
            if let XReferenceEntry::Free(u) = entry {
                Some(u)
            } else {
                None
            }
        })
    }

    pub fn entries(&self) -> impl Iterator<Item = &XReferenceEntry> {
        self.entries.iter()
    }
}

impl std::ops::Deref for XSectionReference {
    type Target = Vec<XReferenceEntry>;

    fn deref(&self) -> &Self::Target {
        &self.entries
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct FreeObject {
    /// Number of this object
    pub number: usize,
    /// Next generation number that should be used
    pub generation: usize,
    /// Next free object number
    pub next_free: usize,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct UsedObject {
    /// Number of this object
    pub number: usize,
    /// The position of this object in the pdf file in bytes, starting from the
    /// beginning of the PDF.
    pub byte_offset: usize,
    /// Next generation number that should be used
    pub generation: usize,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct UsedCompressedObject {
    /// Number of this object
    pub number: usize,
    /// The number of the stream object that contains this object
    pub containing_object: usize,
    /// Index of the object in the object streams
    pub index: usize,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Unsupported {
    /// Number of this object
    pub number: usize,
    /// The number of the stream object that contains this object
    pub type_num: usize,
    /// The number of the stream object that contains this object
    pub w1: usize,
    /// Next generation number that should be used
    pub w2: usize,
}

/// Denotes a free object reference in a xref stream.
pub const XREF_FREE: usize = 0;
/// Denotes a used object reference in a xref stream.
pub const XREF_USED: usize = 1;
/// Denotes a used and compressed object reference in a xref stream.
pub const XREF_COMPRESSED: usize = 2;

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum XReferenceEntry {
    Free(FreeObject),
    Used(UsedObject),
    /// Object is stored in compressed stream
    UsedCompressed(UsedCompressedObject),

    /// Unsupported xref entry. Point to null object.
    Unsupported(Unsupported),
}
impl XReferenceEntry {
    pub fn type_num(&self) -> usize {
        match self {
            XReferenceEntry::Free(_) => XREF_FREE,
            XReferenceEntry::Used(_) => XREF_USED,
            XReferenceEntry::UsedCompressed(_) => XREF_COMPRESSED,
            XReferenceEntry::Unsupported(Unsupported { type_num, .. }) => *type_num,
        }
    }

    pub fn number(&self) -> usize {
        match self {
            XReferenceEntry::Free(FreeObject { number, .. }) => *number,
            XReferenceEntry::Used(UsedObject { number, .. }) => *number,
            XReferenceEntry::UsedCompressed(UsedCompressedObject { number, .. }) => *number,
            XReferenceEntry::Unsupported(Unsupported { number, .. }) => *number,
        }
    }
}

impl From<Unsupported> for XReferenceEntry {
    fn from(v: Unsupported) -> Self {
        Self::Unsupported(v)
    }
}

impl From<UsedCompressedObject> for XReferenceEntry {
    fn from(v: UsedCompressedObject) -> Self {
        Self::UsedCompressed(v)
    }
}

impl From<UsedObject> for XReferenceEntry {
    fn from(v: UsedObject) -> Self {
        Self::Used(v)
    }
}

impl From<FreeObject> for XReferenceEntry {
    fn from(v: FreeObject) -> Self {
        Self::Free(v)
    }
}
